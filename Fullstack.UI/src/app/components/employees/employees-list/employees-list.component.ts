import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  employees: Employee[] = [
    /*{
      id: '5asd341d-ju6r-897w-jgad-asd32rhhur09',
      name: 'John Doe',
      email: 'john.doe@email.com',
      phone: 9745309287,
      salary: 60000,
      department: 'Human Resources'
    },

    {
      id: '5asd351d-ju6r-897w-jgad-asd32rhhur09',
      name: 'Sameer Saini',
      email: 'sameer.saini@gmail.com',
      phone: 9745309286,
      salary: 50000,
      department: 'Information Technologies'
    },

    {
      id: '5asd361d-ju6r-897w-jgad-asd32rhhur09',
      name: 'John Doe',
      email: 'sameer.saini@gmail.com',
      phone: 9745309285,
      salary: 86000,
      department: 'Software Developer'
    }*/
  ];

  constructor(private employeesService: EmployeesService) { }

  ngOnInit(): void {
    this.employeesService.getAllEmployees()
    .subscribe({
      next: (response) => {
        this.employees = response;
      },
      error: (response) => {
        console.log(response);
      }
    })
  }

}
